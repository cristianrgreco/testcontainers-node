const fetch = require('node-fetch');
const { GenericContainer } = require('testcontainers');

describe('testcontainers dind', () => {
  jest.setTimeout(180000);

  it('should work', async () => {
    const container = await new GenericContainer('cristianrgreco/testcontainer', '1.1.12')
      .withExposedPorts(8080)
      .start();

    const url = `http://${container.getHost()}:${container.getMappedPort(8080)}`;
    const response = await fetch(`${url}/hello-world`);
    expect(response.status).toBe(200);

    await container.stop();
  });
});

